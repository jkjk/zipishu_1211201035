<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<title>紫皮书</title>		
		<!--                       CSS                       -->
	  
		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="./css/reset.css" type="text/css" media="screen" />
	  
		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="./css/style.css" type="text/css" media="screen" />
		
		<!--                       Javascripts                       -->
  
		<!-- jQuery -->
		<script type="text/javascript" src="./js/jquery-1.3.2.min.js"></script>
		
		<script type="text/javascript" src="./js/zps.js"></script>
		
	</head>
  
	<body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
		
		<div id="header">
			<div id="header-left" class="align-left">
				<div id="logo">
					<h1>紫皮书</h1>
				</div>
			</div>
			<div id="header-right" class="align-right">
				<div id="header-right-up">
					<div id="slogan" class="align-left"><p>精彩你生活</p></div>
					<div id="search-box" class="align-right">
						<span>
							<input type="text" class="input_text_focus" name="pname">
						</span>
						<input name="emotion" type="submit" value="搜索" class="btn_search">
					</div>
				</div>
				<div id="main-menu">
					<ul class="nav">
						<li id="li-index" ><a href="index.jsp">首页</a></li>
						<li id="li-info" class="on"><a href="#">城市情报</a></li>
						<li id="li-map" ><a href="emotion.jsp">情绪地图</a></li>
						<li id="li-shortcut"><a href="shortcut.jsp">休闲快捷</a></li>
						<li id="li-vogue"><a href="#">时尚中人</a></li>
					</ul>
					<!-- End subnav -->
				</div>
			</div>
			<div class="clear"></div>
		</div>	
		
		<div id="main-body">
		<%@include file="left.jsp"%>
		
		
		<div>
			<div >
				<div style="padding:10px;">
					<div style="padding:0px 0px 10px 0px;"><span><i>◆</i></span>目录：<a href="index.jsp">首页</a>&gt;城市情报</div>
					<img src="images/ads_2.png" width="800" height="90"/>
					
					<%
						int pageNum = 1;	//当前第1页
						int pageSize =4;   //每页几条记录
						int recordCount;
						int pageCount;
						String strPageNum = request.getParameter("pagenum");
						String strAreaId = request.getParameter("areaid");
						
						if(null == strPageNum || "".equals(strPageNum)){
							
						}else{
							pageNum = Integer.parseInt(strPageNum);
						}
								
						Integer areaid = Integer.parseInt(strAreaId);	
						
						Connection dbc = db.getConnection();
						//Statement stmt = dbc.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY); //没有参数用这个
						String sql = "select title,content,source,author from news where areaid=?";
						PreparedStatement pstmt = dbc.prepareStatement(sql,ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);//有参数用这个
						pstmt.setInt(1, areaid);
						ResultSet rs = pstmt.executeQuery();
						rs.last();
						recordCount = rs.getRow();
						
						if(recordCount % pageSize == 0){
							pageCount = recordCount / pageSize;
						}else{
							pageCount = recordCount / pageSize + 1;
						}
						rs.absolute((pageNum-1)*pageSize);
						int i = 0;
						while(rs.next()&& i < pageSize){
					%>
						<tr class="alt-row">
							<h2><center><td><%=rs.getString("title")%></a></td></center></h2>
							<td><%=rs.getString("content")%></td>
							<B>地点：<B/><td><%=rs.getString("source")%></td>&nbsp;&nbsp;&nbsp;&nbsp;
							作者：<td><%=rs.getString("author")%></td>
						</tr>
					<%
						i++;}
						rs.close();
						dbc.close();
					%>
				</div>

			</div>



										
										<div style="float:right;margin:20px 60px 0px 0px;">
											<%if(pageNum != 1){ %>
											<a href="emotion.jsp" title="首页">&laquo; 首页&nbsp;&nbsp;&nbsp;&nbsp;</a>
											<a href="emotion.jsp?pagenum=<%= pageNum-1 %>" title="前一页">&laquo; 前一页&nbsp;&nbsp;&nbsp;&nbsp;</a>
											<%} %>
											<!-- <a href="#" class="number" title="1">1</a>
											<a href="#" class="number" title="2">2</a>
											<a href="#" class="number current" title="3">3</a>
											<a href="#" class="number" title="4">4</a>-->
											<%if(pageNum != pageCount){ %>
											<a href="emotion.jsp?pagenum=<%= pageNum+1 %>" title="后一页">后一页 &raquo;&nbsp;&nbsp;&nbsp;&nbsp;</a>
											<a href="emotion.jsp?pagenum=<%= pageCount %>" title="末页">末页 &raquo;</a>
											<%} %>
										</div> <!-- End .pagination -->
		</div>
		
		<%@include file="bottom.jsp"%>