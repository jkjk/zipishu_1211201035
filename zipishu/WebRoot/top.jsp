<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<title>紫皮书</title>		
		<!--                       CSS                       -->
	  
		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="./css/reset.css" type="text/css" media="screen" />
	  
		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="./css/style.css" type="text/css" media="screen" />
		
		<!--                       Javascripts                       -->
  
		<!-- jQuery -->
		<script type="text/javascript" src="./js/jquery-1.3.2.min.js"></script>
		
		<script type="text/javascript" src="./js/zps.js"></script>
		
	</head>
  
	<body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
		
		<div id="header">
			<div id="header-left" class="align-left">
				<div id="logo">
					<h1>紫皮书</h1>
				</div>
			</div>
			<div id="header-right" class="align-right">
				<div id="header-right-up">
					<div id="slogan" class="align-left"><p>精彩你生活</p></div>
					<div id="search-box" class="align-right">
						<span>
							<input type="text" class="input_text_focus" name="pname">
						</span>
						<input type="submit" value="搜索" class="btn_search">
					</div>
				</div>
				<div id="main-menu">
					<ul class="nav">
						<li id="li-index"  class="on"><a href="index.jsp">首页</a></li>
						<li id="li-info"><a href="City_news.jsp">城市情报</a></li>
						<li id="li-map"><a href="emotion.jsp">情绪地图</a></li>
						<li id="li-shortcut"><a href="shortcut.jsp">休闲快捷</a></li>
						<li id="li-vogue"><a href="#">时尚中人</a></li>
					</ul>
					<!-- 需要就写，不需要则可以不写 -->
					<!-- End subnav -->
				</div>
			</div>
			<div class="clear"></div>
		</div>	
		
		<div id="main-body">