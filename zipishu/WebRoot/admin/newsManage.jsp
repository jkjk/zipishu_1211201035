<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<%@include file="menu.jsp"%>

<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>

<jsp:useBean id="db" class="com.zipishu.DataBaseConnection"></jsp:useBean>

<%
  	String strNewsId = request.getParameter("newsid");
	int newsid = 0;
	String strTitle = "";
	String strSubTitle = "";
	String strAuthor = "";
	String strSource = "";
	Integer areaId = 0;
	Integer catalogId = 0;
	String strContent = "";
	
	if(null == strNewsId || "".equals(strNewsId)){

	}else{
		newsid = Integer.parseInt(strNewsId);
		Connection dbcNews = db.getConnection();
		String sqlNews = "SELECT * FROM news where newsid=?";
		PreparedStatement stmtNews = dbcNews.prepareStatement(sqlNews);
		stmtNews.setInt(1, newsid);
		ResultSet rsNews = stmtNews.executeQuery();
		if(rsNews.next()){
			strTitle = rsNews.getString("title");
			strSubTitle = rsNews.getString("subtitle");
			strAuthor = rsNews.getString("author");
			strSource = rsNews.getString("source");
			strContent = rsNews.getString("content");
			areaId = rsNews.getInt("areaid");
			catalogId = rsNews.getInt("catalogid");
		}
	}
  
	Connection dbcArea = db.getConnection();
	Statement stmtArea = dbcArea.createStatement();
	String sqlArea = "SELECT * FROM area";
	ResultSet rsArea = stmtArea.executeQuery(sqlArea);

	Connection dbcCata = db.getConnection();
	Statement stmtCata = dbcCata.createStatement();
	String sqlCata = "SELECT * FROM catelog";
	ResultSet rsCata = stmtCata.executeQuery(sqlCata);

%>
	<!-- Page Head -->
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>添加事件</h3>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					<div class="notification information png_bg">
							<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<div>
								随便提醒点什么, 注意事项
							</div>
					</div>
					
					<form id="newsForm" action="newsManage_action.jsp" method="post">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p>
									<label>事件标题</label>
									<input class="text-input large-input" type="text" id="title" name="title" value="<%= strTitle %>"/>
								</p>
								<p>
									<label>副标题</label>
									<input class="text-input medium-input datepicker" type="text" id="subtitle" name="subtitle" value="<%= strSubTitle %>"/> <!-- <span class="input-notification error png_bg">错误信息</span> -->
								</p>
								<p>
									<label>作者</label>
										<input class="text-input small-input" type="text" id="author" name="author" value="<%= strAuthor %>"/> <!-- <span class="input-notification success png_bg">成功信息</span> Classes for input-notification: success, error, information, attention
										<br /><small>这个字段的简要描述</small> -->
								</p>
								
								<p>
									<label>来源</label>
										<input class="text-input small-input" type="text" id="source" name="source" value="<%= strSource %>"/> 
								</p>
																
								<p>
									<label>所属分类</label>              
									<select name="catalogid" class="small-input">
									<% while(rsCata.next()){ %>
										<option 

										<%
										if(rsCata.getInt("catalogid") == catalogId){
											out.print(" selected='selected'");
										}
										%>


										 value="<%=rsCata.getInt("catalogid")%>"><%=rsCata.getString("catalogname")%></option>
									<% } %>
									</select> 
								</p>

								<p>
									<label>所属地区</label>              
									<select name="areaid" class="small-input">
									<% while(rsArea.next()){ %>
										<option

										<%
										if(rsArea.getInt("areaid") == areaId){
											out.print(" selected='selected'");
										}
										%>

										 value="<%=rsArea.getInt("areaid")%>"><%=rsArea.getString("areaname")%></option>
									<% } %>
									</select> 
								</p>
								
								<p>
									<label>事件内容</label>
									<!-- <textarea class="text-input textarea wysiwyg" id="content1" name="content1" cols="79" rows="15"></textarea> -->
									
									<script id="content" type="text/plain" style="width:100%;height:200px;"><%=strContent%></script>

								</p>
								
								<p>
								<%if(newsid == 0){%>
									<input class="button" type="submit" value="添加事件" />
								<%}else{%>
									<input type="hidden" name="newsid" value="<%=newsid%>" />
									<input class="button" type="submit" value="修改事件" />
								<%}%>								
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
					<!-- <div class="notification information png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							一条信息
						</div>
					</div>
					
					<div class="notification success png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							成功信息
						</div>
					</div>
					
					<div class="notification attention png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							警告信息
						</div>
					</div>
					
					<div class="notification error png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							错误信息
						</div>
					</div> -->
				</div>
			</div>

		<%
		rsCata.close();
		rsArea.close();
		dbcCata.close();
		dbcArea.close();
		%>


		<script type="text/javascript">
			
			$(document).ready(function(){
				
				/*如果是没有子菜单的选中，如下
				$('#admin-index').addClass('current');*/
				
				/*如果含有子菜单的，请选中父菜单和子菜单，如下*/
				$('#admin-events').addClass('current');
				$('#admin-events').parent().find("ul").slideToggle("slow");
				$('#admin-events-add').addClass('current');
				
				 ///实例化编辑器
			    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
			    var ue = UE.getEditor('content');
				
			});
		</script>


