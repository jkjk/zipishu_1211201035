<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<%

request.setCharacterEncoding("utf-8");


String strAction = request.getParameter("action");
String strNewsId = request.getParameter("newsid");

String title = request.getParameter("title");
String subtitle = request.getParameter("subtitle");
String author = request.getParameter("author");
String source = request.getParameter("source");
String strareaid = request.getParameter("areaid");
String strcatalogid = request.getParameter("catalogid");
String content1 = request.getParameter("editorValue");

/////判空操作//////
if(null == title || "".equals(title)){
	out.println("标题不能为空");
}
//////////



com.zipishu.DataBaseConnection db = new com.zipishu.DataBaseConnection();
Connection con = db.getConnection();
String sql="";
if("delete".equals(strAction)){
    sql = "DELETE FROM news WHERE newsid=?";
	Integer newsid = Integer.parseInt(strNewsId);
	PreparedStatement pstmt = con.prepareStatement(sql);
	pstmt.setInt(1, newsid);
	pstmt.executeUpdate();
}else{
    int areaid = Integer.parseInt(strareaid);
    int catalogid = Integer.parseInt(strcatalogid);
    if(null == strNewsId || "".equals(strNewsId)){
	    
		sql = "INSERT INTO news(title,subtitle,author,source,areaid,catalogid,content,createdate,hit) VALUES (?,?,?,?,?,?,?,getdate(),0)";
	    PreparedStatement pstmt = con.prepareStatement(sql);
	    pstmt.setString(1, title);
	    pstmt.setString(2, subtitle);
	    pstmt.setString(3, author);
	    pstmt.setString(4, source);
	    pstmt.setInt(5, areaid);
	    pstmt.setInt(6, catalogid);
	    pstmt.setString(7, content1);
	    pstmt.executeUpdate();
	    }else{
	    Integer newsid = Integer.parseInt(strNewsId);
		sql = "UPDATE news SET title=?,subtitle=?,author=?,source=?,areaid=?,catalogid=?,content=? WHERE newsid=?";
		PreparedStatement pstmt = con.prepareStatement(sql);
		pstmt.setString(1, title);
		pstmt.setString(2, subtitle);
		pstmt.setString(3, author);
		pstmt.setString(4, source);
		pstmt.setInt(5, areaid);
		pstmt.setInt(6, catalogid);
		pstmt.setString(7, content1);	
		pstmt.setInt(8,newsid);
	    pstmt.executeUpdate();
	    }
	}


con.close();

response.sendRedirect("newsList.jsp");

%>

