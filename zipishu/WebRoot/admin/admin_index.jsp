<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%@include file="menu.jsp"%>

<!-- Page Head -->
			
			<h2>欢迎你,<%=adminname %></h2>
			<p id="page-intro">想管理点什么?</p>
			
			<ul class="shortcut-buttons-set">
				
				<li><a class="shortcut-button" href="item.jsp"><span>
					<img src="./images/icons/pencil_48.png" alt="icon" /><br />
					类别管理
				</span></a></li>
				
				<li><a class="shortcut-button" href="itemManage.jsp"><span>
					<img src="./images/icons/paper_content_pencil_48.png" alt="icon" /><br />
					添加类别
				</span></a></li>
				
				<li><a class="shortcut-button" href="newsList.jsp"><span>
					<img src="./images/icons/image_add_48.png" alt="icon" /><br />
					事件管理
				</span></a></li>
				
				<li><a class="shortcut-button" href="newsManage.jsp"><span>
					<img src="./images/icons/clock_48.png" alt="icon" /><br />
					添加事件
				</span></a></li>
				
				<li><a class="shortcut-button" href="destinationList.jsp"><span>
					<img src="./images/icons/comment_48.png" alt="icon" /><br />
					地点管理
				</span></a></li>
				
			</ul><!-- End .shortcut-buttons-set -->
			
			<div class="clear"></div> <!-- End .clear -->
			
			
			
			<div class="content-box column-left">
				
				<div class="content-box-header">
					
					<h3>管理须知</h3>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab">
					
						<p>
							blablabla..<br/>
							走的时候别忘了登出.
						</p>
						
					</div> <!-- End #tab3 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->
			
			<div class="content-box column-right">
				
				<div class="content-box-header"> <!-- Add the class "closed" to the Content box header to have it closed by default -->
					
					<h3>关于</h3>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					
					<div class="tab-content default-tab">
					
						<h4>还有标题的</h4>
						<p>
							可以放一些联系方式
						</p>
						
					</div> <!-- End #tab3 -->        
					
				</div> <!-- End .content-box-content -->
				
			</div> <!-- End .content-box -->


		<script type="text/javascript">
			
			$(document).ready(function(){
				
				/*如果是没有子菜单的选中，如下*/
				$('#admin-index').addClass('current');
				
				/*如果含有子菜单的，请选中父菜单和子菜单，如下
				$('#admin-events').addClass('current');
				$('#admin-events').parent().find("ul").slideToggle("slow");
				$('#admin-events-add').addClass('current');*/
				
			});
		</script>


<%@include file="footer.jsp"%>