<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<%@include file="menu.jsp"%>
<jsp:useBean id="db" class="com.zipishu.DataBaseConnection"></jsp:useBean>
		<!-- Page Head -->
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>事件管理</h3>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					<div class="notification information png_bg">
							<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<div>
								随便提醒点什么, 注意事项.
							</div>
					</div>
					<!-- Data Table -->
					<table>
							
							<thead>
								<tr>
								   <th><input class="check-all" type="checkbox" /></th>
								   <th>标题</th>
								   <th>分类</th>
								   <th>作者</th>
								   <th>地区</th>
								   <th>发表时间</th>
								   <th>点击</th>
								   <th>操作</th>
								</tr>
								
							</thead>
						 
						 
							<tbody>
								
								

	<%
	int pageNum = 1;	//当前第1页
	int pageSize = 10; //每页15条记录
	int recordCount;
	int pageCount;
	String strPageNum = request.getParameter("pagenum");
	if(null == strPageNum || "".equals(strPageNum)){
		
	}else{
		pageNum = Integer.parseInt(strPageNum);
	}
		
	
	Connection dbc = db.getConnection();
	//Statement stmt = dbc.createStatement();
	String sql = "SELECT n.newsid,n.title, n.author, n.hit, n.createdate, a.areaname, c.catalogname FROM news AS n, area AS a, catelog AS c WHERE n.areaid=a.areaid AND n.catalogid=c.catalogid";
	PreparedStatement pstmt = dbc.prepareStatement(sql, ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
	ResultSet rs = pstmt.executeQuery();
	rs.last();
	recordCount = rs.getRow();
	
	if(recordCount % pageSize == 0){
		pageCount = recordCount / pageSize;
	}else{
		pageCount = recordCount / pageSize + 1;
	}
	rs.absolute((pageNum-1)*pageSize);
	int i = 0;
	while(rs.next() && i < pageSize){
	%>

		<tr>
			<td><input type="checkbox" /></td>
			<td><%=rs.getString("title")%></td>
			<td><%=rs.getString("catalogname")%></td>
			<td><%=rs.getString("author")%></td>
			<td><%=rs.getString("areaname")%></td>
			<td><%=rs.getDate("createdate")%></td>
			<td><%=rs.getInt("hit")%></td> 
			<td>
				<!-- Icons -->
				 <a href="newsManage.jsp?newsid=<%=rs.getInt("newsid")%>" title="Edit"><img src="./images/icons/pencil.png" alt="Edit" /></a>
				 <a href="newsManage_action.jsp?action=delete&newsid=<%=rs.getInt("newsid")%>" title="Delete"><img src="./images/icons/cross.png" alt="Delete" /></a> 
			</td>
		</tr>

		
		
		
		
		
		
		<br/>
	<%
		i++;
		
	}
	rs.close();
	dbc.close();
	%>
							</tbody>
							<tfoot>
								<tr>
									<td colspan="8">
										<div class="bulk-actions align-left">
											<select name="dropdown">
												<option value="option1">请选择操作...</option>
												<option value="option2">编辑</option>
												<option value="option3">删除</option>
											</select>
											<a class="button" href="#">选中的项</a>
										</div>
										
										<div class="pagination">
											<%if(pageNum != 1){ %>
											<a href="newsList.jsp" title="首页">&laquo; 首页</a>
											<a href="newsList.jsp?pagenum=<%= pageNum-1 %>" title="前一页">&laquo; 前一页</a>
											<%} %>
											<!-- <a href="#" class="number" title="1">1</a>
											<a href="#" class="number" title="2">2</a>
											<a href="#" class="number current" title="3">3</a>
											<a href="#" class="number" title="4">4</a>-->
											<%if(pageNum != pageCount){ %>
											<a href="newsList.jsp?pagenum=<%= pageNum+1 %>" title="后一页">后一页 &raquo;</a>
											<a href="newsList.jsp?pagenum=<%= pageCount %>" title="末页">末页 &raquo;</a>
											<%} %>
										</div> <!-- End .pagination -->
										<div class="clear"></div>
									</td>
								</tr>
							</tfoot>
							
							
							
						</table>
						<!-- End Table -->
					</div>
			</div>


		<script type="text/javascript">
			
			$(document).ready(function(){
				
				/*如果是没有子菜单的选中，如下
				$('#admin-index').addClass('current');*/
				
				/*如果含有子菜单的，请选中父菜单和子菜单，如下*/
				$('#admin-events').addClass('current');
				$('#admin-events').parent().find("ul").slideToggle("slow");
				$('#admin-events-manage').addClass('current');
				
			});
		</script>



<%@include file="footer.jsp"%>