<%@ page language="java" import="java.util.*,java.sql.*" pageEncoding="utf-8"%>
<%@include file="menu.jsp"%>
<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.config.js"></script>
<script type="text/javascript" charset="utf-8" src="../ueditor/ueditor.all.min.js"> </script>
<!--建议手动加在语言，避免在ie下有时因为加载语言失败导致编辑器加载失败-->
<!--这里加载的语言文件会覆盖你在配置项目里添加的语言类型，比如你在配置项目里配置的是英文，这里加载的中文，那最后就是中文-->
<script type="text/javascript" charset="utf-8" src="../ueditor/lang/zh-cn/zh-cn.js"></script>

<jsp:useBean id="db" class="com.zipishu.DataBaseConnection"></jsp:useBean>
<%

	String strDesid = request.getParameter("desid");
	int desid = 0;
	String strName = "";
	String strAddress = "";
	String strTelephone = "";
	int strAvgconsume = 0;
	Integer areaId = 0;
	Integer timeId = 0;
	Integer emotionId = 0;
	Integer shortId = 0;
    int parkstate = 0;
	String strBusguide = "";
	String strRecommend = "";
	String strAbout = "";

	if(null == strDesid || "".equals(strDesid)){

	}else{
		desid = Integer.parseInt(strDesid);
		Connection dbcNews = db.getConnection();
		String sqlNews = "SELECT * FROM destination where desid=?";
		PreparedStatement stmtNews = dbcNews.prepareStatement(sqlNews);
		stmtNews.setInt(1, desid);
		ResultSet rsNews = stmtNews.executeQuery();
		if(rsNews.next()){
			strName = rsNews.getString("name");
			strAddress = rsNews.getString("address");
			strTelephone = rsNews.getString("telephone");
			strAvgconsume = rsNews.getInt("avgconsume");
			areaId = rsNews.getInt("areaid");
			timeId = rsNews.getInt("timeid");
			emotionId = rsNews.getInt("emotionid");
			shortId = rsNews.getInt("shortid");
			parkstate=rsNews.getInt("parkstate");
			strBusguide = rsNews.getString("busguide");
			strRecommend = rsNews.getString("recommend");
			strAbout = rsNews.getString("about");
		}
	}


	Connection dbcArea = db.getConnection();
	Statement stmtArea = dbcArea.createStatement();
	String sqlArea = "SELECT * FROM area";
	ResultSet rsArea = stmtArea.executeQuery(sqlArea);
	
	Connection dbcBus = db.getConnection();
	Statement stmtBus = dbcBus.createStatement();
	String sqlBus = "SELECT * FROM bustime";
	ResultSet rsBus = stmtBus.executeQuery(sqlBus);
	
	Connection dbcEmo = db.getConnection();
	Statement stmtEmo = dbcEmo.createStatement();
	String sqlEmo = "SELECT * FROM emotion";
	ResultSet rsEmo = stmtEmo.executeQuery(sqlEmo);
	
	Connection dbcSho = db.getConnection();
	Statement stmtSho = dbcSho.createStatement();
	String sqlSho = "SELECT * FROM shortcut";
	ResultSet rsSho = stmtSho.executeQuery(sqlSho);


%>
	<!-- Page Head -->
			<div class="content-box"><!-- Start Content Box -->
				
				<div class="content-box-header">
					
					<h3>添加地点</h3>
					
					<div class="clear"></div>
					
				</div> <!-- End .content-box-header -->
				
				<div class="content-box-content">
					<div class="notification information png_bg">
							<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
							<div>
								随便提醒点什么, 注意事项.
							</div>
					</div>
					
					<form id="newsForm" action="destinationManage_action.jsp" method="post">
							
							<fieldset> <!-- Set class to "column-left" or "column-right" on fieldsets to divide the form into columns -->
								
								<p>
									<label>名称</label>
									<input class="text-input small-input" type="text" id="name" name="name" value="<%= strName %>" />
								</p>
								<p>
									<label>地址</label>
									<input class="text-input small-input datepicker" type="text" id=address name="address"  value="<%= strAddress %>"/> 
								<p>
									<label>电话</label>
										<input class="text-input small-input" type="text" id="telephone" name="telephone"  value="<%= strTelephone %>"/> 
								</p>
								
								<p>
									<label>人均消费</label>
										<input class="text-input small-input" type="text" id="avgconsume" name="avgconsume"  value="<%= strAvgconsume %>"/> 
								</p>
								
								<p>
									<label>所属地区</label>              
									<select name="areaid" class="small-input">
									<% while(rsArea.next()){ %>
										<option

										<%
										if(rsArea.getInt("areaid") == areaId){
											out.print(" selected='selected'");
										}
										%>

										 value="<%=rsArea.getInt("areaid")%>"><%=rsArea.getString("areaname")%></option>
									<% } %>
									</select> 
								</p>
								
								<p>
									<label>营业时间</label>              
									<select name="timeid" class="small-input">
									<% while(rsBus.next()){ %>
										<option 

										<%
										if(rsBus.getInt("timeid") == timeId){
											out.print(" selected='selected'");
										}
										%>


										 value="<%=rsBus.getInt("timeid")%>"><%=rsBus.getString("timename")%></option>
									<% } %>
									</select> 
								</p>
								
								<p>
									<label>所属情绪</label>              
									<select name="emotionid" class="small-input">
									<% while(rsEmo.next()){ %>
										<option

										<%
										if(rsEmo.getInt("emotionid") == emotionId){
											out.print(" selected='selected'");
										}
										%>

										 value="<%=rsEmo.getInt("emotionid")%>"><%=rsEmo.getString("emotionname")%></option>
									<% } %>
									</select> 
								</p>
								
								<p>
									<label>休闲快捷</label>              
									<select name="shortid" class="small-input">
									<% while(rsSho.next()){ %>
										<option

										<%
										if(rsSho.getInt("shortid") == shortId){
											out.print(" selected='selected'");
										}
										%>

										 value="<%=rsSho.getInt("shortid")%>"><%=rsSho.getString("shortname")%></option>
									<% } %>
									</select> 
								</p>

								<p>
									<label>车位</label>
									
									<input class="text-input small-input" type="text" id="parkstate" name="parkstate"  value="<%= parkstate %>"/> 
	
								</p>
								
								<p>
									<label>公交指南</label>
									
									<input class="text-input large-input" type="text" id="busguide" name="busguide"  value="<%= strBusguide %>"/> 

								</p>
								
								<p>
									<label>友情贴士</label>
									
									<input class="text-input large-input" type="text" id="recommend" name="recommend"  value="<%= strRecommend %>"/> 

								</p>
								
								<p>
									<label>简介</label>
									<!-- <textarea class="text-input textarea wysiwyg" id="content1" name="content1" cols="79" rows="15"></textarea> -->
									
									<script id="content" type="text/plain" style="width:100%;height:200px;"><%=strAbout%></script>

								</p>
								
								<p>
								<%if(desid == 0){%>
									<input class="button" type="submit" value="添加地点" />
								<%}else{%>
									<input type="hidden" name="desid" value="<%=desid%>" />
									<input class="button" type="submit" value="修改地点" />
								<%}%>
								</p>
								
							</fieldset>
							
							<div class="clear"></div><!-- End .clear -->
							
						</form>
					<!-- <div class="notification information png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							一条信息
						</div>
					</div>
					
					<div class="notification success png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							成功信息
						</div>
					</div>
					
					<div class="notification attention png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							警告信息
						</div>
					</div>
					
					<div class="notification error png_bg">
						<a href="#" class="close"><img src="./images/icons/cross_grey_small.png" title="Close this notification" alt="close" /></a>
						<div>
							错误信息
						</div>
					</div> -->
				</div>
			</div>

		<%
		rsArea.close();
		rsBus.close();
		rsEmo.close();
		rsSho.close();
		dbcArea.close();
		dbcBus.close();
		dbcEmo.close();
		dbcSho.close();
		%>

		<script type="text/javascript">
			
			$(document).ready(function(){
				
				/*如果是没有子菜单的选中，如下
				$('#admin-index').addClass('current');*/
				
				/*如果含有子菜单的，请选中父菜单和子菜单，如下*/
				$('#admin-place').addClass('current');
				$('#admin-place').parent().find("ul").slideToggle("slow");
				$('#admin-place-add').addClass('current');




			    //实例化编辑器
			    //建议使用工厂方法getEditor创建和引用编辑器实例，如果在某个闭包下引用该编辑器，直接调用UE.getEditor('editor')就能拿到相关的实例
			    var ue = UE.getEditor('content');

			    //ue.setContent("<%=strAbout%>");
				
			});


		</script>
<%@include file="footer.jsp"%>