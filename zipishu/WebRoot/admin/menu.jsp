<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%
String adminname = (String)session.getAttribute("ADMIN");
if(null == adminname){
response.sendRedirect("login.jsp");
return;
}
 %>

<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
		
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
		
		<title>紫皮书 后台管理</title>
		
		<!--                       CSS                       -->
	  
		<!-- Reset Stylesheet -->
		<link rel="stylesheet" href="./css/reset.css" type="text/css" media="screen" />
	  
		<!-- Main Stylesheet -->
		<link rel="stylesheet" href="./css/style.css" type="text/css" media="screen" />
		
		<!-- Invalid Stylesheet. This makes stuff look pretty. Remove it if you want the CSS completely valid -->
		<link rel="stylesheet" href="./css/invalid.css" type="text/css" media="screen" />	
		
		<!-- Colour Schemes
	  
		Default colour scheme is green. Uncomment prefered stylesheet to use it.
		
		<link rel="stylesheet" href="./css/blue.css" type="text/css" media="screen" />
		
		<link rel="stylesheet" href="./css/red.css" type="text/css" media="screen" />  
	 
		-->
		
		<!-- Internet Explorer Fixes Stylesheet -->
		
		<!--[if lte IE 7]>
			<link rel="stylesheet" href="./css/ie.css" type="text/css" media="screen" />
		<![endif]-->
		
		<!--                       Javascripts                       -->
  
		<!-- jQuery -->
		<script type="text/javascript" src="./js/jquery-1.3.2.min.js"></script>
		
		<!-- jQuery Configuration -->
		<script type="text/javascript" src="./js/simpla.jquery.configuration.js"></script>
		
		<!-- Facebox jQuery Plugin -->
		<script type="text/javascript" src="./js/facebox.js"></script>
		
		<!-- jQuery WYSIWYG Plugin -->
		<!---->
		<script type="text/javascript" src="./js/jquery.wysiwyg.js"></script>
		
		<!-- jQuery Datepicker Plugin -->
		<!--
		<script type="text/javascript" src="./js/jquery.datePicker.js"></script>
		<script type="text/javascript" src="./js/jquery.date.js"></script>
		-->
		<!--[if IE]><script type="text/javascript" src="./js/jquery.bgiframe.js"></script><![endif]-->

		
		<!-- Internet Explorer .png-fix -->
		
		<!--[if IE 6]>
			<script type="text/javascript" src="./js/DD_belatedPNG_0.0.7a.js"></script>
			<script type="text/javascript">
				DD_belatedPNG.fix('.png_bg, img, li');
			</script>
		<![endif]-->
  

		
	</head>
  
	<body><div id="body-wrapper"> <!-- Wrapper for the radial gradient background -->
		
		<div id="sidebar"><div id="sidebar-wrapper"> <!-- Sidebar with logo and menu -->
			
			<h1 id="sidebar-title"><a href="#">紫皮书 后台管理</a></h1>
		  
			<!-- Logo (221px wide) -->
			<a href="#"><img id="logo" src="./images/logo.png" alt="Simpla Admin logo" /></a>
		  
			<!-- Sidebar Profile links -->
			<div id="profile-links">
				欢迎你, <a href="#" title="Edit your profile"><%=adminname %></a>
				, 你有<a href="#" title="3 Messages">3条消息</a><br />
				<br />
				<a href="#" title="View the Site">查看站点</a> | <a href="#" title="Sign Out">登出</a>
			</div>        
			
			<ul id="main-nav">  <!-- Accordion Menu -->
				
				<li>
					<a href="admin_index.jsp" id="admin-index" class="nav-top-item no-submenu"> <!-- Add the class "no-submenu" to menu items with no sub menu -->
						管理首页
					</a>       
				</li>
				
				<li> 
					<a href="#" id="admin-class" class="nav-top-item"> <!-- Add the class "current" to current menu item -->
						类别管理
					</a>
					<ul>
						<li><a id="admin-class-add" href="itemManage.jsp">添加类别</a></li>
						<li><a id="admin-class-manage" href="item.jsp">管理类别</a></li> <!-- Add class "current" to sub menu items also -->
					</ul>
				</li>
				
				<li>
					<a href="#" id="admin-events" class="nav-top-item">
						事件管理
					</a>
					<ul>
						<li><a id="admin-events-add" href="newsManage.jsp">添加事件</a></li>
						<li><a id="admin-events-manage" href="newsList.jsp">事件管理</a></li>
					</ul>
				</li>
				
				<li>
					<a href="#" id="admin-place" class="nav-top-item">
						地点管理
					</a>
					<ul>
						<li><a id="admin-place-add" href="destinationManage.jsp">添加地点</a></li>
						<li><a id="admin-place-manage" href="destinationList.jsp">管理地点</a></li>
					</ul>
				</li>
				
			</ul> <!-- End #main-nav -->
			
		</div></div> <!-- End #sidebar -->
		
		<div id="main-content"> <!-- Main Content Section with everything -->
			
			<noscript> <!-- Show a notification if the user has disabled javascript -->
				<div class="notification error png_bg">
					<div>
						你的浏览器禁用了Javascript, 请启用, ,否则正常功能无法使用
					</div>
				</div>
			</noscript>