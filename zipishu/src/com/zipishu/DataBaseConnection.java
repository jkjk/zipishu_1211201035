package com.zipishu;

import java.sql.*;

public class DataBaseConnection {

	private Connection con;

	/**
	 * 返回一个数据库的连接。 如果要达到对数据库连接统一控制的目的，此处需要设为静态方法。
	 */
	public Connection getConnection() {
		
		// sqlserver 2005
	    String driver = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
		String url = "jdbc:sqlserver://localhost:1072;DatabaseName=zps";
		String user = "sa";
		String password = "123";

		try {
			Class.forName(driver);
		} catch (java.lang.ClassNotFoundException e) {
			e.printStackTrace();
		}

		try {
			con = DriverManager.getConnection(url, user, password);
		} catch (SQLException ex) {
			ex.printStackTrace();
		}
		return con;
	}

	/* 关闭数据库连接 */
	public void close() {
		try {
			con.close();
		} catch (SQLException sqle) {
			sqle.printStackTrace();
		}
	}
}